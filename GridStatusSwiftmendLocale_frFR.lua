﻿--[[--------------------------------------------------------------------
	GridStatusSwiftmend_frFR.lua
	French localization for GridStatusSwiftmend.
----------------------------------------------------------------------]]

if GetLocale() ~= "frFR" then return end
local _, GridStatusSwiftmend = ...
GridStatusSwiftmend.L = {
	["Swiftmend is ready"] = "Prompte guérison est prête",
	["Swiftmend will be ready"] = "Prompte guérison sera prête",
	["Swiftmend is ready and urgent"] = "Prompte guérison est prête et urgente!",	
	["Color of the swiftmend status, if swiftmend is ready"] = "Couleur du status quand la prompte guérison est prête",
	["Health threshold : Swiftmend is urgent"] = "Seuil d'activation en %",
	["DESC: Health threshold : Swiftmend is urgent"] = "Seuil d'activation en % pour indiquer que la prompte guérison est urgente pour sauver le joueur",
	["Show the remaining time"]	= "Afficher le temps restant",
	["Color of the swiftmend status, if swiftmend will be ready"] = "Couleur du status quand la prompte guérison sera prête",
	["Color of the swiftmend status, if swiftmend is ready and urgent"] = "Couleur du status quand la prompte guérison est prête et urgente",
	["Show the remaining time, else the text Swiftmend! if swiftmend is ready and urgent"] = "Afficher le temps restant sinon le texte 'prompte guérison' si elle est prête et urgente",
	["Just show if Wild Growth is off Cooldown"] = "Il suffit de montrer si Croissance sauvage est éteint Cooldown",
	["Just show Swiftmend indicator if Wild Growth is off Cooldown and talent Soul of the Forest is selected!"] = "Il suffit de montrer indicateur Prompte si Croissance sauvage est hors Cooldown et le talent d'âme de la forêt est sélectionnée !",	
}
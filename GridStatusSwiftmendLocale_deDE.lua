﻿--[[--------------------------------------------------------------------
	GridStatusSwiftmend_deDE.lua
	German (Deutsch) localization for GridStatusSwiftmend.
----------------------------------------------------------------------]]

if GetLocale() ~= "deDE" then return end
local _, GridStatusSwiftmend = ...
GridStatusSwiftmend.L = {
	["Swiftmend is ready"] = "Rasche Heilung ist bereit",
	["Swiftmend will be ready"] = "Rasche Heilung wird rechtzeitig bereit sein",
	["Swiftmend is ready and urgent"] = "Rasche Heilung ist bereit und dringend erforderlich",
	["Color of the swiftmend status, if swiftmend is ready"] = "Farbe des Status für Rasche Heilung, wenn Rasche Heilung bereit ist",
	["Health threshold : Swiftmend is urgent"] = "Schwellenwert der prozentualen Gesundheit, ab welcher Rasche Heilung als dringend angezeigt werden soll",
	["DESC: Health threshold : Swiftmend is urgent"] = "Schwellenwert der prozentualen Gesundheit, ab welcher Rasche Heilung als dringend angezeigt werden soll",
	["Show the remaining time"]	= "Zeige Restzeit",
	["Color of the swiftmend status, if swiftmend will be ready"] = "Farbe des Status für Rasche Heilung, wenn Rasche Heilung rechtzeitig bereit sein wird",	
	["Color of the swiftmend status, if swiftmend is ready and urgent"] = "Farbe des Status für Rasche Heilung, wenn Rasche Heilung bereit und dringend erforderlich ist",
	["Show the remaining time, else the text Swiftmend! if swiftmend is ready and urgent"] = "Zeige entweder die Restzeit oder den Text Swiftment!, wenn Rasche Heilung bereit und dringend erforderlich ist",	
	["Just show if Wild Growth is off Cooldown"] = "Zeigen Sie einfach , wenn Wildwuchs ist Abklingzeit",
	["Just show Swiftmend indicator if Wild Growth is off Cooldown and talent Soul of the Forest is selected!"] = "Nur zeigen Rasche Heilung Indikator , wenn Wildwuchs aus Aufladezeit ist und Talent Seele des Waldes ausgewählt !",
}
﻿--[[--------------------------------------------------------------------
	GridStatusSwiftmend.lua
----------------------------------------------------------------------]]

local _, GridStatusSwiftmend = ...

if not GridStatusSwiftmend.L then GridStatusSwiftmend.L = { } end

local L = setmetatable(GridStatusSwiftmend.L, {
	__index = function(t, k)
		t[k] = k
		return k
	end
})

--}}}

local spellNameCache = {};
spellNameCache.Swiftmend = GetSpellInfo(18562);
spellNameCache.WildGrowth = GetSpellInfo(48438);

local GridStatusSwiftmend = Grid:GetModule("GridStatus"):NewModule("GridStatusSwiftmend", "AceTimer-3.0")
GridStatusSwiftmend.menuName = "GridStatusSwiftmend";
local GridRoster = Grid:GetModule("GridRoster");
local refreshTimer;

GridStatusSwiftmend.defaultDB = {
	alert_cooldown_swiftmend = {
		text = spellNameCache["Swiftmend"],
		enable = true,
		color = { r = 0, g = 1, b = 0, a = 1 },	
		priority = 70,
		range = true,
		color2 = { r = 0, g = 0.2, b = 0, a = 1 },
		healthThreshold = 100,
		color3 = { r = 1, g = 1, b = 1, a = 1 },
		showtime = true,	
	}
}

local options = {
	["color"] = {
		type = "color",
		name = L["Swiftmend is ready"],
		desc = L["Color of the swiftmend status, if swiftmend is ready"],
		hasAlpha = true,
		get = function ()
			local color = GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.color
			return color.r, color.g, color.b, color.a
		end,
		set = function (_,r, g, b, a)
			local color = GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.color
			color.r = r
			color.g = g
			color.b = b
			color.a = a or 1
		end,
	},
	["color2"] = {
		type = "color",
		name = L["Swiftmend will be ready"],
		desc = L["Color of the swiftmend status, if swiftmend will be ready"],
		hasAlpha = true,
		get = function ()
			local color2 = GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.color2
			return color2.r, color2.g, color2.b, color2.a
		end,
		set = function (_,r, g, b, a)
			local color2 = GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.color2
			color2.r = r
			color2.g = g
			color2.b = b
			color2.a = a or 1
		end,
	},
	["health threshold"] = {
	type = "range",
	name = L["Health threshold : Swiftmend is urgent"],
	desc = L["DESC: Health threshold : Swiftmend is urgent"],
	max=100,
	min=0,
	step = 1,
	get = function ()
		return GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.healthThreshold
	end,
	set = function (_,v)
		GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.healthThreshold = v
	end,
	},
	["color3"] = {
		type = "color",
		name = L["Swiftmend is ready and urgent"],
		desc = L["Color of the swiftmend status, if swiftmend is ready and urgent"],
		hasAlpha = true,
		get = function ()
			local color3 = GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.color3
			return color3.r, color3.g, color3.b, color3.a
		end,
		set = function (_,r, g, b, a)
			local color3 = GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.color3
			color3.r = r
			color3.g = g
			color3.b = b
			color3.a = a or 1
		end,
	},
	["wild growth is up"] = {
		type = "toggle",
		name = L["Just show if Wild Growth is off Cooldown"],
		desc = L["Just show Swiftmend indicator if Wild Growth is off Cooldown and talent Soul of the Forest is selected!"],
		get = function() 
			return GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.wgisup 
		end,
		set = function(_,v)
			GridStatusSwiftmend.db.profile.alert_cooldown_swiftmend.wgisup = v
		end,
	}	
}


GridStatusSwiftmend.options = false

function GridStatusSwiftmend:OnInitialize()

	self.super.OnInitialize(self)
	self:RegisterStatus("alert_cooldown_swiftmend", spellNameCache["Swiftmend"], options, true)
end


function GridStatusSwiftmend:Reset()
	self.super.Reset(self)
end

function GridStatusSwiftmend:OnEnable()
	refreshTimer = self:ScheduleRepeatingTimer("UpdateAll", 0)
end

function GridStatusSwiftmend:UpdateAll()
	for guid, unitid in GridRoster:IterateRoster() do
        self:updateUnit(self, guid, unitid)
	end
end

function isTrulyOnCd (spell)
    local gcdStart, gcdDuration = GetSpellCooldown(61304);
    local cdStart, cdDuration = GetSpellCooldown(spell);

    return (cdDuration>gcdDuration);
end

function GridStatusSwiftmend:updateUnit(self, gridid, unitid)
	local id,_,_,hasSotf,_ = GetTalentInfo(5, 1, 1);
	local health = (UnitHealth(unitid) / UnitHealthMax(unitid))*100;
	local textToShow="SW";
	local settings = self.db.profile.alert_cooldown_swiftmend
	local wildGrowthOnCd = isTrulyOnCd(spellNameCache["WildGrowth"]);
	local swiftmendOnCd = isTrulyOnCd(spellNameCache["Swiftmend"]);
	local wildGrowthRestriction = false;

	wildGrowthRestriction = hasSotf and settings.wgisup;

    if (((wildGrowthOnCd or swiftmendOnCd) and wildGrowthRestriction) or (swiftmendOnCd)) then
        self.core:SendStatusGained(gridid, "alert_cooldown_swiftmend",settings.priority,(settings.range and 40),settings.color2,textToShow);
    elseif (not wildGrowthOnCd and not swiftmendOnCd and wildGrowthRestriction) or (not swiftmendOnCd) then
        if health>settings.healthThreshold then
            self.core:SendStatusGained(gridid, "alert_cooldown_swiftmend",settings.priority,(settings.range and 40),settings.color,textToShow);
        else
            self.core:SendStatusGained(gridid, "alert_cooldown_swiftmend",settings.priority,(settings.range and 40),settings.color3,textToShow);
        end
    else 
        self.core:SendStatusLost(gridid, "alert_cooldown_swiftmend")
    end
end

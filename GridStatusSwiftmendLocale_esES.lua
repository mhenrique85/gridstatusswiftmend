﻿--[[--------------------------------------------------------------------
	GridStatusSwiftmend_esES.lua
	Spanish localization for GridStatusSwiftmend.
----------------------------------------------------------------------]]

if GetLocale() ~= "esES" then return end
local _, GridStatusSwiftmend = ...
GridStatusSwiftmend.L = {
	["Swiftmend is ready"] = "Swiftmend is ready",
	["Swiftmend will be ready"] = "Swiftmend will be ready",
	["Swiftmend is ready and urgent"] = "Swiftmend is ready and urgent",
	["Color of the swiftmend status, if swiftmend is ready"] = "Color of the swiftmend status, if swiftmend is ready",
	["Health threshold : Swiftmend is urgent"] = "% Health threshold : Swiftmend is urgent",
	["DESC: Health threshold : Swiftmend is urgent"] = "% Health threshold : Swiftmend is urgent",
	["Show the remaining time"]	= "Show the remaining time",
	["Color of the swiftmend status, if swiftmend will be ready"] = "Color of the swiftmend status, if swiftmend will be ready",
	["Color of the swiftmend status, if swiftmend is ready and urgent"] = "Color of the swiftmend status, if swiftmend is ready and urgent",
	["Show the remaining time, else the text Swiftmend! if swiftmend is ready and urgent"] = "Show the remaining time, else the text Swiftmend! if swiftmend is ready and urgent",
	["Just show if Wild Growth is off Cooldown"] = "Solo tiene que mostrar si Crecimiento salvaje está apagado Enfriamiento",
	["Just show Swiftmend indicator if Wild Growth is off Cooldown and talent Soul of the Forest is selected!"] = "Solo tiene que mostrar indicador de Alivio presto si Crecimiento salvaje está fuera de enfriamiento y se selecciona Alma talento del Bosque !",
}